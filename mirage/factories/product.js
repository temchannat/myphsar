import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  name(i) {
    return faker.commerce.productName()
  },

  price: 34.6,

  imageUrl() {
    return faker.internet.avatar()
  },

  description() {
    return faker.lorem.paragraph()
  },

  location() {
    return faker.address.city()
  },

  date() {
    return faker.date.past()
  },

  numberOfViews() {
    return faker.random.number()
  }

});
