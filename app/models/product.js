import DS from 'ember-data';

const {attr} = DS

export default DS.Model.extend({

  name: attr('string'),
  priceFixed: attr('string'),
  planState: attr('string'),
  slug: attr('string'),
  imageUrl: attr('string'),
  description: attr('string'),
  location: attr('string'),
  date: attr('date'),
  numberOfViews: attr('number'),
  attributes: attr()
});
