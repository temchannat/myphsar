import DS from 'ember-data';

const {attr, hasMany } = DS

export default DS.Model.extend({
  slug: attr('string'),
  name: attr('string'),
  icon: hasMany('icon'),
  isRoot: attr('boolean'),
  childrenCount: attr('number'),
  postsCount: attr('number'),
  attributes: attr()

});
