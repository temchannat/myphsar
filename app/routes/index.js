import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    var categories = this.store.findAll('category')
    var products = this.store.findAll('product')
    var data  = {
      "categories": categories,
      "products": products
    }
    return data
  }

});
