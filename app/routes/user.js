import Route from '@ember/routing/route';

export default Route.extend({

  users: [
    {name: 'Channat', gender: 'M', address: 'Phnom Penh'},
    {name: 'Makara', gender: 'F', address: 'Svay Rieng'},
    {name: 'Pkay', gender: 'F', address: 'Svay Rieng'},
    {name: 'Kimsoer', gender: 'M', address: 'Kampong Cham'},
    {name: 'Sida', gender: 'M', address: 'Kampong Cham'},
  ],

  actions: {
    exportToExcelUser() {
      let csvRows = []
      // get the headers
      let headers = Object.keys(this.users[0])
      csvRows.push(headers.join(','))

      // loop over the rows
      for (const row of this.users) {
        const values = headers.map(header => {
          const escaped = (""+row[header]).replace(/"/g, '\\"')
          return `"${escaped}"`
        })
        csvRows.push(values.join(','))
        // console.log("===> ", values)
      }
      const objectToCSV = csvRows.join('\n')

      // starting to download
      const blob = new Blob([objectToCSV], {type: 'text/csv'})
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a')
      a.setAttribute('hidden', '')
      a.setAttribute('href', url)
      a.setAttribute('download', 'myphsar_users.csv')
      document.body.appendChild(a)
      a.click()
      document.body.removeChild(a)
   }
  },
  model() {
    return this.users
  }
});
