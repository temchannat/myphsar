import DS from 'ember-data';

export default DS.JSONAPIAdapter.extend({
    host: "http://52.221.123.79/api/api/v1/categories/?locale=en&fields%5Bcategory%5D=name%2Cslug%2Cicon%2CchildrenCount%2CpostsCount%2CisRoot%2CancestryTree&filter%5Bis_root%5D=true&page%5Benabled%5D=false",
    headers: {
      Authorization: "123"
    }
});
