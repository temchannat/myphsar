import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  host: "http://52.221.123.79/api/api/v1/posts/feed?locale=en&fields%5Bcategory%5D=name%2Cslug&fields%5Bgallery%5D=cover_image&fields%5Bpost%5D=name%2Cprice-fixed%2Cgallery%2Ccurrency%2Cgiveaway%2Cfeatured%2Cplan-state%2Cslug&include=gallery.cover_image%2Ccategory%2Ccurrency&page%5Bsize%5D=18&reload=true",
  headers: {
    Authorization: "123"
  }
});
