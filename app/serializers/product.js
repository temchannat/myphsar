import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    let product_list = []
    payload.data.map( pro => {
      const data = {
        id: pro.id,
        name: pro.attributes.name,
        priceFixed: pro.attributes['price-fixed'],
        planState: pro.attributes['plan-state'],
        slug: pro.attributes.slug
      }
      product_list.push(data)
    })
    //console.log("==> ", product_list)
    payload = {
      products: product_list
    };
    return this._super(store, primaryModelClass, payload, id, requestType);
  }
});
