export default ExportCSV =  function download_as_csv(data, fileName) {
  let csvRows = []
  // get the headers
  let headers = Object.keys(data[0])
  csvRows.push(headers.join(','))

  // loop over the rows
  for (const row of data) {
    const values = headers.map(header => {
      const escaped = (""+row[header]).replace(/"/g, '\\"')
      return `"${escaped}"`
    })
    csvRows.push(values.join(','))
    // console.log("===> ", values)
  }
  const objectToCSV = csvRows.join('\n')

  // starting to download
  const blob = new Blob([objectToCSV], {type: 'text/csv'})
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement('a')
  a.setAttribute('hidden', '')
  a.setAttribute('href', url)
  a.setAttribute('download', fileName)
  document.body.appendChild(a)
  a.click()
  document.body.removeChild(a)
}
